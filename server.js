const url = require('url');
const fs = require('fs');
const uuid = require("uuid");
const { instrument } = require("@socket.io/admin-ui");
const { createAdapter } = require("@socket.io/redis-adapter");
const { createClient } = require("redis");
const User = require('./class/User.js');
const Room = require('./class/Room.js');
const UserController = require('./class/UserController.js');
const RoomController = require('./class/RoomController.js');
const StrapiController = require('./class/StrapiController.js');
const AuthenController = require('./class/AuthenController.js');
const DiscordController = require('./class/DiscordController.js');

var host = '0.0.0.0';
var port = 3000;
const passphrase = 'Ptcg2@222';
const http = require('http').createServer();
const io = require('socket.io')(http, {
    transports: [ 'polling', 'websocket'],
    // reconnection: true, 
    // upgrade: false,
    cors: {
        origin: ['https://ptcg.world', 'https://ptcg-game.ptcg.world', 'https://room.ptcg.world', 'https://room-dev.ptcg.world', 'https://ptcg-game-dev.ptcg.world', 'http://localhost:8080', 'http://localhost:3010', 'http://localhost:5500', 'https://socketioadmin.z23.web.core.windows.net'],
        methods: ["GET", "POST"],
        credentials: true
    }
});
let redisUrl = process.env.SERVER_URL || "redis://localhost:6379"
const pubClient = createClient({ url: redisUrl });
const subClient = pubClient.duplicate();

Promise.all([pubClient.connect(), subClient.connect()]).then(() => {
    console.log('redis connected: ' + redisUrl);
    io.adapter(createAdapter(pubClient, subClient));
    //   io.listen(3000);
});

instrument(io, {
    auth: false
});

var rooms = new RoomController(notifyRoomStatusChanged);
var onlineUser = new UserController();

var pendingRemoveRooms = new Map();
let discordBotId = null;

io.on('connection', function (socket) {
    OnConnection(socket);
});

http.listen(port, host, { perMessageDeflate: false }, function () {
    console.log('Server started! Listening on: http://' + host + ':' + port);
});


setInterval(() => {
    checkEmptyRoom();
}, 500);

let auth = new AuthenController('https://strapi.ptcg.world');
let strapi = new StrapiController('https://strapi.ptcg.world');
let discordApi = new DiscordController('https://discord.com');

// auth.getJwtToken().then(jwt => {
//     strapi.Debug('demo-deck', true, jwt).then(deck => console.log(JSON.stringify(deck)))
//     strapi.ImportCardFromTxt('./data/all_2.txt', jwt)
// })
// strapi.GetDeck('demo-deck', true).then(deck => console.log(JSON.stringify(deck)))

// const { readFileSync, promises: fsPromises } = require('fs')
// let format = readFileSync('./data/import1.txt', 'utf-8');
// let data1 = readFileSync('./data/import2.txt', 'utf-8');
// let data2 = data1.split('\n')
// let output = '';
// for (let index = 0; index < data2.length; index++) {
//     let data = data2[index].split(' ');
//     data[0] = data[0].replace('(', '').replace(')', '')
//     let pack = data[0];
//     // data = data.slice(1)
//     let name = data.join(' ')

//     let f = format
//     f = f.replace('$name', name.trim())
//     f = f.replace('$value', pack)
//     output += f
// }
// console.log(output)

let deckNames = ['叉字幅deck', '刺龍王帕路deck', '帕路奇亞deck', '恥鬼一擊deck', '賽場用帕路奇亞deck', '賽場用索羅deck', '賽場用夢夢deck', '賽場用夢夢deck2', '翹翹用達仔deck']
deckNames.forEach(deckName => {
    try {
        let json = require(`./data/${deckName}.json`);
        // strapi.createDeckFromOldJson(json, deckName)
    } catch (error) {

    }
});

function OnConnection(socket) {
    console.log('User connected: ' + socket.id);

    let user = onlineUser.createUser(socket.id);

    //  Bind function
    bindBasicEvent(socket);
    bindGameEvent(socket);

    notifyRoomStatusChanged();
}

function bindBasicEvent(socket) {
    socket.on('disconnect', () => {
        onDisconnect(socket);
    });
    socket.on('heatbeat', function (data) {
        onHeatbeat(socket, data);
    });
    socket.on('set name', function (data) {
        onSetName(socket, data);
    });
    socket.on('create room', function (data) {
        onCreateRoom(socket, data);
    });
    socket.on('join room', function (data) {
        onJoinRoom(socket, data);
    });
    socket.on('leave room', function (data) {
        onLeaveRoom(socket, data);
    });
    socket.on('registerDiscordBot', function (data) {
        onRegisterDiscordBot(socket, data);
    });
    socket.on('discord - create game room', function (data) {
        onDiscordCreateGameRoom(socket, data);
    });
    socket.on('discord code', function (data) {
        onDiscordCode(socket, data);
    });
}

function bindGameEvent(socket) {
    socket.on('action', function (actionName, effect, obj, isPlayerA) {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('action', actionName, effect, obj, isPlayerA);
            }
        }
    });

    socket.on('dealCards', function () {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('dealCards');
            }
        }
    });
    socket.on('startGame', function (canStart, isPlayerA) {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('startGame', canStart, isPlayerA);
            }
        }
    });
    socket.on('flipCards', function () {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('flipCards');
            }
        }
    });
    socket.on('initDeck', function () {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('initDeck');
            }
        }
    });
    socket.on('drawCard', function (isPlayerA) {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('drawCard', isPlayerA);
            }
        }
    })
    socket.on('endTurn', function (isPlayerA) {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('endTurn', isPlayerA);
            }
        }
    })
    socket.on('cardPlayed', function (gameObject, isPlayerA, emitAction) {
        let user = onlineUser.findUser(socket.id);
        if (user?.room) {
            let room = rooms.findRoom(user.room);
            if (room) {
                io.to(room.id).emit('cardPlayed', gameObject, isPlayerA, emitAction);
            }
        }
    });
}

function onDisconnect(socket) {
    let user = onlineUser.findUser(socket.id);
    onlineUser.removeUser(user);
    if (user.room) {
        let room = rooms.findRoom(user.room);
        if (room) {
            room.removeUser(user);
            addToPendingRemoveRoom(room);
        }
    }

    console.log('User disconnected: ' + socket.id + ' ' + user.name);
}

function onHeatbeat(socket) {
    socket.emit('heatbeat', { 'date': new Date() });
}

function onSetName(socket, data) {
    let user = onlineUser.findUser(socket.id);
    let oldName = user.name;
    user.name = data.name;
    console.log(`User:${user.id} changed name from ${oldName} to ${user.name}`);
    notifyRoomStatusChanged();
}

function onCreateRoom(socket, data) {
    let user = onlineUser.findUser(socket.id);
    if (!user) {
        socket.emit('create room result', {
            'result': 'failed',
            'reason': 'Error: 找不用戶: {' + socket.id + '}',
        });
        return;
    }
    if (user.room) {
        socket.emit('create room result', {
            'result': 'failed',
            'reason': '你已經在另一個房間: ' + user.room.name,
        });
        return;
    }

    let room = rooms.createRoom(generateUid(), data?.password);
    if (user.name) {
        room.name = user.name + "的房間";
        rooms.roomUpdatedCallback(room);
    }
    addToPendingRemoveRoom(room);

    let content = {
        'result': 'success',
        'token': room.id,
    }
    if (room.password !== undefined) {
        content.password = room.password
    }
    console.log(`Created Room: ${room.id} Password: ${content.password}`);
    socket.emit('create room result', content);
    return;
}

function joinRoom(socket, roomId, password) {
    let user = onlineUser.findUser(socket.id);
    if (user.room) {
        return `你已經加入了另一個房間 ${user.room}`;
    }
    let room = rooms.findRoom(roomId);

    if (!room) {
        return `房間已失效`;
    }

    if (room.password !== undefined) {
        if (room.password !== password) {
            return `房間密碼錯誤`;
        }
    }

    room.addUser(user);
    user.room = room.id;
    socket.join(room.id);

    io.in(room.id).emit('user join', {
        'user': user,
    });

    console.log(`User: ${user.id} joined Room: ${room.id} Password: ${room.password}`)
    return room;
}

function onJoinRoom(socket, data) {
    if (!data?.token) {
        socket.emit('join room result', {
            'result': 'failed',
            'reason': '無效的房間代碼.',
        });
        return;
    }

    let joinRoomResult = joinRoom(socket, data.token, data.password);
    if (joinRoomResult?.id === undefined) {
        socket.emit('join room result', {
            'result': 'failed',
            'reason': joinRoomResult,
        });
        return;
    }

    let joinedRoomUser = [];
    joinRoomResult.users.forEach((value, key) => {
        joinedRoomUser.push({
            id: key,
            name: value.name,
        });
    })
    console.log(`joinedRoomUser: ${JSON.stringify(joinedRoomUser)}`);

    let content = {
        'result': 'success',
        'token': joinRoomResult.id,
        'password': joinRoomResult.password,
        'roomName': joinRoomResult.name,
        'isPlayerA': joinRoomResult.users.size === 1,
        'users': joinedRoomUser,
    }
    if (joinRoomResult.password !== undefined) {
        content.password = joinRoomResult.password
    }
    socket.emit('join room result', content);
}

function onRegisterDiscordBot(socket, data) {
    discordBotId = socket.id
    socket.join("discordBot")
    console.log(`Register discord bot: ${discordBotId}`)
}

function onDiscordCreateGameRoom(socket, data) {
    io.to(discordBotId).emit('create game room by invite', data);
}

async function onDiscordCode(socket, data) {
    let code = data.code;
    let redirect_uri = data.redirect_uri;
    const sockets = await io.in("discordBot").fetchSockets();
    sockets.forEach(s => {
        s.emit('get bot config', { fields: ['clientId', 'clientSecret'] });
        s.once('bot config', function (data) {
            let clientId = data.clientId;
            let clientSecret = data.clientSecret;

            discordApi.getDiscordToken(code, clientId, clientSecret, redirect_uri)
                .then(result => {
                    // console.log(`Discord Token: ${result.token_type} ${result.access_token}`);
                    discordApi.getDiscordUser(result.token_type, result.access_token)
                        .then(user => {
                            socket.emit('get server discord user', { user: user })
                            socket.emit('discord code result', { user: user })
                        })
                })
        })
    })
    // io.to(discordBotId).emit('get bot config', { fields: ['clientId', 'clientSecret'] });
    // socket.emit('discord token', data);
}

function addToPendingRemoveRoom(room) {
    if (pendingRemoveRooms.has(room.id)) {
        return;
    }

    pendingRemoveRooms.set(room.id, Date.now());
}

function checkEmptyRoom() {
    pendingRemoveRooms.forEach((value, key) => {
        let room = rooms.findRoom(key);
        if (room && room?.users?.size > 0) {
            pendingRemoveRooms.delete(key);
        } else if (Date.now() >= value + (60 * 1000)) { // Remove room after time
            pendingRemoveRooms.delete(key);
            rooms.removeRoom(room);
            console.log('Remove room: ' + room.id);
        }
    })
}

function generateUid() {
    let token = uuid.v4();
    return token;
}

function notifyRoomStatusChanged() {
    io.emit('room list', { 'rooms': getAllGameRooms() });
}

function getAllGameRooms() {
    var result = [];
    rooms.rooms.forEach((value, key, map) => {
        let room = value;
        if (room.users.size > 0 || true) {
            var users = [];
            room.users.forEach((value, key, map) => {
                let user = value;
                users.push(user.name);
            });
            result.push(
                {
                    'id': room.id,
                    'name': room.name,
                    'users': users,
                    'hasPssword': room.password !== undefined,
                });
        }
    });

    return result;
}

function isValidString(str) {
    if (str) {
        var regex = /^[\w\s]+$/;
        if (str.match(regex))
            return true;
    }
    return false;
}

const encrypt = (data) => {
    let json = JSON.stringify(data);
    return btoa(json);
};

const decrypt = (ciphertext) => {
    let originalText = atob(ciphertext);
    let data = JSON.parse(originalText);
    return data;
};