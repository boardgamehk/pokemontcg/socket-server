FROM node:16
COPY ./ .
RUN npm install
RUN npm update
RUN npm audit fix
EXPOSE 3000
CMD ["node", "server.js"]