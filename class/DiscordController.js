class DiscordController {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
        this.https = require('https')
        this.axios = require('axios')
        this.axios = this.axios.create({
            httpsAgent: new this.https.Agent({
                rejectUnauthorized: false
            })
        });
    }

    getDiscordToken(code, clientId, clientSecret, redirect_uri) {
        const params = new URLSearchParams();
        params.append('client_id', clientId);
        params.append('client_secret', clientSecret);
        params.append('grant_type', 'authorization_code');
        params.append('code', code);
        params.append('redirect_uri', redirect_uri);

        return this.axios.post(`${this.baseUrl}/api/oauth2/token`, params, { params: { redirect_uri: redirect_uri } })
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(`An error occurred: ${JSON.stringify(error, this.censor(error))}`);
                console.log(error.response?.data)
                return error.response?.data
            });

    }

    getDiscordUser(tokenType, accessToken) {
        return this.axios.get(`${this.baseUrl}/api/users/@me`, { headers: { Authorization: `${tokenType} ${accessToken}` } })
            .then(response => {
                return response.data
            })
            .catch(error => {
                console.log(`An error occurred: ${JSON.stringify(error, this.censor(error))}`);
                console.log(error.response?.data)
                return error.response?.data
            });

    }

    censor(censor) {
        var i = 0;

        return function (key, value) {
            if (i !== 0 && typeof (censor) === 'object' && typeof (value) == 'object' && censor == value)
                return '[Circular]';

            if (i >= 29) // seems to be a harded maximum of 30 serialized objects?
                return '[Unknown]';

            ++i; // so we know we aren't using the original object anymore

            return value;
        }
    }
}

module.exports = DiscordController;