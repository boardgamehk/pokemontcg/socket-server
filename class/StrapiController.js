class StrapiController {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
        this.https = require('https')
        this.axios = require('axios')
        this.axios = this.axios.create({
            httpsAgent: new this.https.Agent({
                rejectUnauthorized: false
            })
        });
        this.qs = require('qs')
        // this.strapiToken = require('./StrapiToken.json')
    }

    Debug(deckCode, getCardData, jwt) {
        let jsonBody = {
            filters: {
                DeckCode: {
                    $eq: deckCode,
                },
            }
        }
        if (getCardData) {
            jsonBody.populate = {
                cards: {
                    populate: ['Card'],
                }
            }
        }
        let query = this.qs.stringify(jsonBody, {
            encodeValuesOnly: true,
        });
        let url = `${this.baseUrl}/api/decks?${query}`;
        console.log(url);
        return this.axios.get(url, {
            headers: {
                Authorization:
                    `Bearer ${jwt}`,
            }
        })
            .then(res => {
                if (res?.data?.meta?.pagination?.total === 1) {
                    return this.cleanUpStrapiResponse(res.data.data[0], this)
                }
            })
            .catch(error => console.error(error))
    }

    GetDeck(deckCode, getCardData) {
        let jsonBody = {
            filters: {
                DeckCode: {
                    $eq: deckCode,
                },
            }
        }
        if (getCardData) {
            jsonBody.populate = {
                cards: {
                    populate: ['Card'],
                }
            }
        }
        let query = this.qs.stringify(jsonBody, {
            encodeValuesOnly: true,
        });
        let url = `${this.baseUrl}/api/decks?${query}`;
        console.log(url);
        return this.axios.get(url)
            .then(res => {
                if (res?.data?.meta?.pagination?.total === 1) {
                    return this.cleanUpStrapiResponse(res.data.data[0], this)
                }
            })
            .catch(error => console.error(error))
    }

    ImportCardFromTxt(txtFilePath, jwt) {
        const { readFileSync, promises: fsPromises } = require('fs')

        let contents = readFileSync(txtFilePath, 'utf-8');

        contents = contents.replace(']', '')
        let cards = contents.split('[');
        cards.splice(0, 1)

        let interval = 100
        let self = this
        cards.forEach((card, index) => {
            setTimeout(function () {
                card = card.replace(']', '')
                card = card.split('\n')
                let row1 = card[0].split(' ')
                let row2 = card[1].split('.')
                let Name = row1[2]
                let Category = row1[1]
                let Attribute
                switch (Category) {
                    case '支援者':
                        Category = 'supporter'
                        break;
                    case '能量':
                    case '特殊能量':
                        Category = 'energy'
                        break;
                    case '物品':
                        Category = 'item'
                        break;
                    case '寶可夢道具':
                        Category = 'tool'
                        break;
                    case '競技場':
                        Category = 'stadium'
                        break;

                    case '雷':
                        Category = 'pokemon'
                        Attribute = 'Lightning'
                        break;
                    case '超':
                        Category = 'pokemon'
                        Attribute = 'Psychic'
                        break;
                    case '火':
                        Category = 'pokemon'
                        Attribute = 'Fire'
                        break;
                    case '鬥':
                        Category = 'pokemon'
                        Attribute = 'Fighting'
                        break;
                    case '水':
                        Category = 'pokemon'
                        Attribute = 'Water'
                        break;
                    case '鋼':
                        Category = 'pokemon'
                        Attribute = 'Metal'
                        break;
                    case '惡':
                        Category = 'pokemon'
                        Attribute = 'Darkness'
                        break;
                    case '草':
                        Category = 'pokemon'
                        Attribute = 'Grass'
                        break;
                    case '龍':
                        Category = 'pokemon'
                        Attribute = 'Dragon'
                        break;
                    case '妖':
                        Category = 'pokemon'
                        Attribute = 'Fairy'
                        break;
                    case '無色':
                        Category = 'pokemon'
                        Attribute = 'Colorless'
                        break;

                    default:
                        Category = 'error'
                        break;
                }
                let CardId = row2[0]
                let Image = `https://hkbgstor.blob.core.windows.net/ptcg-img/${CardId}.jpg`

                //  Check if card exist
                let query = self.qs.stringify({
                    filters: {
                        CardId: {
                            $eq: CardId,
                        },
                    },
                }, {
                    encodeValuesOnly: true,
                });
                let headers = {
                    headers: {
                        Authorization:
                            `Bearer ${jwt}`,
                    }
                }
                let url = `${self.baseUrl}/api/cards?${query}`;
                self.axios.get(url, headers)
                    .then(res => {
                        let count = res?.data?.meta?.pagination?.total
                        if (count === 0) {
                            //  Create new card

                            let postBody = {
                                headers: {
                                    Authorization:
                                        `Bearer ${jwt}`,
                                },
                                data: {
                                    CardId: CardId,
                                    Name: Name,
                                    Category: Category,
                                    Attribute: Attribute,
                                    Image: Image,
                                }
                            }
                            let url = `${self.baseUrl}/api/cards`;
                            self.axios.post(url, postBody, headers)
                                .then(res => {
                                    let newCard = self.cleanUpStrapiResponse(res.data, self)
                                    console.log(`Added card: ${newCard.CardId} ${newCard.Name}`)
                                })
                                .catch(error => {
                                    console.log(`Failed to add card: ${CardId} ${Name}`)
                                    console.log(`${error}`)
                                    console.log(`${JSON.stringify(postBody)}`)
                                })

                        } else {
                            let dbCard = self.cleanUpStrapiResponse(res.data.data[0], self)
                            if (dbCard.CardId != CardId ||
                                dbCard.Name != Name ||
                                dbCard.Category != Category ||
                                dbCard.Attribute != Attribute ||
                                dbCard.Image != Image) {
                                //  Update image new card

                                // console.log(`${JSON.stringify(dbCard)}`)
                                // console.log(`${dbCard.CardId} ${CardId}`)
                                // console.log(`${dbCard.Name} ${Name}`)
                                // console.log(`${dbCard.Attribute} ${Attribute}`)
                                // console.log(`${dbCard.Category} ${Category}`)
                                // console.log(`${dbCard.Image} ${Image}`)

                                let putBody = {
                                    data: {
                                        CardId: CardId,
                                        Name: Name,
                                        Category: Category,
                                        Attribute: Attribute,
                                        Image: Image,
                                    }
                                }
                                let url = `${self.baseUrl}/api/cards/${dbCard.id}`;
                                self.axios.put(url, putBody, headers)
                                    .then(res => {
                                        let updatedCard = self.cleanUpStrapiResponse(res.data, self)
                                        console.log(`Updated card: ${updatedCard.CardId} ${updatedCard.Name}`)
                                    })
                                    .catch(error => {
                                        console.log(`Failed to update card: ${CardId} ${Name}`)
                                        console.log(`${error}`)
                                        console.log(`${JSON.stringify(putBody)}`)
                                    })
                            } else {
                                console.log(`${CardId} already exist`)
                            }
                        }
                    })
                    .catch(error => {
                        console.error(`${CardId} error ---------------------------------------------`)
                        console.error(`${error}`)
                        console.log(`${JSON.stringify(getBody)}`)
                    })

                // if (Category === 'error' || Name === undefined || CardId === undefined || Image === undefined) {
                //     console.log(card);
                //     console.log(Name);
                //     console.log(Category);
                // }
            }, index * interval);
        });

    }

    cleanUpStrapiResponse(data, self) {
        if (data === null || data === undefined || typeof data === 'string') {
            return data;
        }

        if (Array.isArray(data)) {
            for (let i = 0; i < data.length; i++) {
                data[i] = self.cleanUpStrapiResponse(data[i], self)
            }
        } else {
            data = self.cleanUpStrapiNode(data, self)
            Object.keys(data).forEach(function (key) {
                data[key] = self.cleanUpStrapiResponse(data[key], self)
            });
        }
        return data
    }

    cleanUpStrapiNode(data, self) {
        let cleanUped = false;
        let cleanUpList = ['attributes', 'data']
        cleanUpList.forEach(key => {
            if (data[key] != undefined) {
                Object.keys(data[key]).forEach(function (innerKey) {
                    data[innerKey] = data[key][innerKey]
                });
                data[key] = undefined
                cleanUped = true;
            }
        });

        if (cleanUped) {
            data = self.cleanUpStrapiNode(data, self)
        }

        return data;
    }

    getExpandedDeck(deck) {
        let deckClone = JSON.parse(JSON.stringify(deck))
        let rawData = []
        deckClone.cards.forEach(card => {
            for (let i = 0; i < card.Count; i++) {
                rawData.push(card.Card)
            }
        });
        deckClone.rawData = rawData
        return deckClone
    }

    createDeckFromOldJson(json, deckName) {
        let self = this;
        let uniqueCardMap = new Map();
        let checkedCount = 0

        json.forEach(x => {
            x.cardId = x.image.match(/(?<=ptcg-img\/)(.*)(?=.jpg)/)[0]
            if (!uniqueCardMap.has(x.cardId)) {
                uniqueCardMap.set(x.cardId, x)
            }
            let cardData = uniqueCardMap.get(x.cardId);
            if (cardData.count === undefined) {
                cardData.count = 0
            }
            cardData.count += 1
        })
        uniqueCardMap.forEach(x => {
            //  Check if card exist
            let query = self.qs.stringify({
                filters: {
                    CardId: {
                        $eq: x.cardId,
                    },
                },
            }, {
                encodeValuesOnly: true,
            });
            let url = `${self.baseUrl}/api/cards?${query}`;
            self.axios.get(url)
                .then(res => {
                    let count = res?.data?.meta?.pagination?.total
                    if (count === 0) {
                        //  Create new card

                        let postBody = {
                            data: {
                                CardId: x.cardId,
                                Name: x.name,
                                Category: x.category,
                                Image: x.image,
                            }
                        }
                        let url = `${self.baseUrl}/api/cards`;
                        self.axios.post(url, postBody)
                            .then(res => {
                                let newCard = self.cleanUpStrapiResponse(res.data, self)
                                newCard.count = x.count
                                uniqueCardMap.set(newCard.CardId, newCard)
                                console.log(`Added card: ${newCard.CardId} ${newCard.Name}`)
                                self.createDeckFromOldJson_CallBack(self, uniqueCardMap, deckName, ++checkedCount)
                            })
                            .catch(error => {
                                console.log(`Failed to add card: ${x.cardId} ${x.name}`)
                                console.log(`${error}`)
                                console.log(`${JSON.stringify(postBody)}`)
                            })

                    } else {
                        let card = self.cleanUpStrapiResponse(res.data.data[0], self)
                        card.count = x.count
                        uniqueCardMap.set(card.CardId, card)
                        self.createDeckFromOldJson_CallBack(self, uniqueCardMap, deckName, ++checkedCount)
                    }
                })
                .catch(error => {
                    console.error(error)
                })

        });
    }

    createDeckFromOldJson_CallBack(self, uniqueCardMap, deckName, checkedCount) {
        if (uniqueCardMap.size === checkedCount) {
            self.createDeck(self, uniqueCardMap, deckName)
        }
    }

    createDeck(self, uniqueCardMap, deckName) {
        console.log(`Creating deck: ${deckName}`)

        let cards = []
        uniqueCardMap.forEach(x => {
            let card = {
                Count: x.count,
                Card: {
                    id: x.id
                }
            }
            cards.push(card)
        })
        let postBody = {
            data: {
                Name: deckName,
                DeckCode: self.generateDeckCode(self),
                cards: cards
            }
        }
        // console.log(`${JSON.stringify(postBody)}`)

        let url = `${this.baseUrl}/api/decks`;
        self.axios.post(url, postBody)
            .then(res => {
                let newDeck = self.cleanUpStrapiResponse(res.data, self)
                console.log(`Added deck: ${newDeck.Name} ${newDeck.DeckCode}`)
            })
            .catch(error => {
                console.log(`Failed to add deck: ${postBody.data.Name}`)
                console.log(`${error}`)
                console.log(`${JSON.stringify(postBody)}`)
            })
    }

    generateDeckCode(self) {
        let slug = self.generateUID()
        return slug
    }

    generateUID() {
        var firstPart = (Math.random() * 46656) | 0;
        var secondPart = (Math.random() * 46656) | 0;
        firstPart = ("000" + firstPart.toString(36)).slice(-3);
        secondPart = ("000" + secondPart.toString(36)).slice(-3);
        return firstPart + secondPart;
    }

}

module.exports = StrapiController;