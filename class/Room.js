const User = require('./User.js');

class Room {
    constructor(id, roomUpdatedCallback) {
        this.id = id;
        this.users = new Map();
        this.roomUpdatedCallback = roomUpdatedCallback;
    }

    findUser(userId) {
        return this.users.get(userId);
    }

    addUser(user) {
        if (this.users.has(user.id)) return;

        this.users.set(user.id, user);
        this.roomUpdatedCallback(user);
    }

    removeUser(user) {
        this.users.delete(user.id);
        this.roomUpdatedCallback(user);
    }
}

module.exports = Room;