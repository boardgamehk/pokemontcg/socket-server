const User = require('./User.js');

class UserController {
    constructor() {
        this.users = new Map();
    }

    findUser(userId) {
        return this.users.get(userId);
    }

    addUser(user) {
        if (this.users.has(user.id)) return;

        this.users.set(user.id, user);
    }

    removeUser(user) {
        this.users.delete(user.id);
    }

    createUser(userId) {
        let user = new User(userId);
        user.name = user.id;
        this.addUser(user);
        return user;
    }
}

module.exports = UserController;