class AuthenController {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
        this.https = require('https')
        this.axios = require('axios')
        this.axios = this.axios.create({
            httpsAgent: new this.https.Agent({
                rejectUnauthorized: false
            })
        });
        this.strapiToken = require('./StrapiToken.json')
    }

    getJwtToken() {
        console.log(`${JSON.stringify(this.strapiToken)}`)
        let url = `${this.baseUrl}/api/auth/local`
        console.log(url)
        return this.axios
            .post(url, this.strapiToken)
            .then((response) => {
                // Handle success.
                // console.log(`Data: ${response.data.jwt}`);
                return response.data.jwt
            })
            .catch((error) => {
                // Handle error.
                console.log(`An error occurred: ${error.response}`);
            });
    }

}

module.exports = AuthenController;