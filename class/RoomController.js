const Room = require('./Room.js');

class RoomController {
    constructor(roomUpdatedCallback) {
        this.rooms = new Map();
        this.roomUpdatedCallback = roomUpdatedCallback;
    }

    findRoom(roomId) {
        return this.rooms.get(roomId);
    }

    addRoom(room) {
        if (this.rooms.has(room.id)) return;

        this.rooms.set(room.id, room);
        this.roomUpdatedCallback(room);
    }

    removeRoom(room) {
        this.rooms.delete(room.id);
        this.roomUpdatedCallback(room);
    }

    createRoom(roomId, password) {
        let room = new Room(roomId, this.roomUpdatedCallback);
        room.name = room.id;
        if (password !== undefined) {
            room.password = password;
        }
        this.addRoom(room);
        return room;
    }
}

module.exports = RoomController;